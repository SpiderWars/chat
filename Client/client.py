import socket, threading

# TODO -> create client class and define his properties
# TODO -> create _create_client_socket function
# TODO -> create _send_message function
# TODO -> create _listen_for_messages function (function that listen in thread to messages from the server)
# TODO -> add all our project to GIT (create master branch, dev branch)
HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8888         # The port used by the server
class Client:
    def __init__(self) -> None:
        self.client_socket = None
        self._create_client_socket()
        self.nickname = input("Type a nickname: ")
        self.listen_for_messages_thread = threading.Thread(target=self._listen_for_messages)
        self.listen_for_messages_thread.start()

        self.listen_for_user_input_thread = threading.Thread(target=self._send_message)
        self.listen_for_user_input_thread.start()


    def _create_client_socket(self):
        """
        Initialize the client socket
        :return socket: This function will return client socket
        """
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((HOST, PORT))


    def _send_message(self):
        self.client_socket.send((self.nickname + " Is connected :)").encode())
        while True:
            message = input(">> ")
            if message.lower().strip() == "exit":
                print("Exiting")
                self.client_socket.send((self.nickname + " has exited this conversation").encode())
                self.client_socket.close()
                break
            else: self.client_socket.send(( self.nickname + ": " + message).encode())

    def _listen_for_messages(self):
        try:
            while True:
                message = self.client_socket.recv(1024)
                print(message.decode(), end="\n>> ")
        except ConnectionAbortedError or AttributeError:
            pass
        


if __name__ == "__main__":
    Client()


# HOST = '127.0.0.1'  # The server's hostname or IP address
# PORT = 8888         # The port used by the server

# with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#     s.connect((HOST, PORT))
#     message = input("Enter message: ")
#     s.sendall(message.encode())
#     data = s.recv(1024)

# print('Received', repr(data))